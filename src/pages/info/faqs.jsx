import React from "react"
import Layout from "../../components/skeleton/Layout"
import DefaultPage from "../../components/defaultPage"

const Faqs = () => {
  return (
    <Layout>
      <DefaultPage pageTitle="FAQs" />
    </Layout>
  )
}

export default Faqs
