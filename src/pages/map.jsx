import React from "react"
import Layout from "../components/skeleton/Layout"
import DefaultPage from "../components/defaultPage"

const Map = () => {
  return (
    <Layout>
      <DefaultPage pageTitle="Maps" />
    </Layout>
  )
}

export default Map
