import React from "react"
import { makeStyles } from "@material-ui/core/styles"

const useStyles = makeStyles(theme => {
  return {
    root: {
      textAlign: "center",
    },
  }
})

const MainContainer = ({ children }) => {
  const className = useStyles()
  return <main className={className.root}>{children}</main>
}

export default MainContainer
