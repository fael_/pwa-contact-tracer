import React from "react"
import Layout from "../../components/skeleton/Layout"
import DefaultPage from "../../components/defaultPage"

const Contact = () => {
  return (
    <Layout>
      <DefaultPage pageTitle="Contact" />
    </Layout>
  )
}

export default Contact
