/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
// import { useStaticQuery, graphql } from "gatsby"
import Helmet from "react-helmet"
import { withPrefix } from "gatsby"
import MainContainer from "./mainContainer"
// import Header from "./Header"
import { ThemeProvider } from "@material-ui/core/styles"
import ThemeGlobal from "../../utilities/themeGlobal"
import CssBaseline from "@material-ui/core/CssBaseline"
import NoSsr from "@material-ui/core/NoSsr"

import "../../scss/index.scss"

const Layout = ({ children }) => {
  // const data = useStaticQuery(graphql`
  //   query SiteTitleQuery {
  //     site {
  //       siteMetadata {
  //         title
  //       }
  //     }
  //   }
  // `)

  return (
    <>
      <NoSsr>
        <ThemeProvider theme={ThemeGlobal}>
          <CssBaseline />
          {/* <Header siteTitle={data.site.siteMetadata.title} pageTitle={pageTitle} /> */}
          <MainContainer>{children}</MainContainer>
        </ThemeProvider>
        <Helmet>
          <script
            async
            defer
            src={withPrefix("js/compressed.js")}
            type="text/javascript"
          />
        </Helmet>
      </NoSsr>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
