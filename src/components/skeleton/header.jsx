import PropTypes from "prop-types"
import React from "react"
import { AppBar, Typography, Toolbar } from "@material-ui/core"
import KeyboardBackspaceOutlinedIcon from '@material-ui/icons/KeyboardBackspaceOutlined';
import { makeStyles } from "@material-ui/core/styles"
import { Link } from 'gatsby';

const useStyles = makeStyles(theme => {
  return {
    root: {
      color: theme.palette.common.white,
      position: 'absolute',
      zIndex: 1,
      "& span": {
        color: theme.palette.common.white,
        verticalAlign: 'middle',
        marginRight: theme.spacing(1.5)
      }
    }
  }
})

const Header = ({ pageTitle }) => {
  const classes = useStyles();

  return(
    <AppBar position="static" className={classes.root}>
      <Toolbar>
        <Link to="/">
          <span><KeyboardBackspaceOutlinedIcon /></span>
        </Link>
        <Typography variant="h6" className={classes.title}>
          {pageTitle}
        </Typography>
      </Toolbar>
    </AppBar>
  );
}

Header.propTypes = {
  pageTitle: PropTypes.string,
}

Header.defaultProps = {
  pageTitle: ``,
}

export default Header
