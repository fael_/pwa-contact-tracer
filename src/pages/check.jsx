import React from "react"
import Layout from "../components/skeleton/Layout"
import DefaultPage from "../components/defaultPage"

const CheckIn = () => {
  return (
    <Layout>
      <DefaultPage pageTitle="Check In" />
    </Layout>
  )
}

export default CheckIn
