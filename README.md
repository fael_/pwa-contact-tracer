# PWA Contact Tracer

![PWA Contact Tracer](/src/images/gatsby-icon.png)

To create a robust, rapidly deployable contact tracing platform to help reduce the spread of the COVID19 Pandemic.

### Tech

- [React JS](https://reactjs.org/) - A JavaScript library for building user interfaces
- [Gatsby JS](https://www.gatsbyjs.org/) - Gatsby is a free and open source framework based on React that helps developers build blazing fast websites and apps
- [Material UI](https://material-ui.com/) - UI library

### Installation

This project requires [Node JS](https://nodejs.org/en/) v12+ to run.

Install the dependencies and devDependencies and start the server.
Note: We have two cli commands `gulp and gatsby` to be installed either globally or locally to the project

```sh
$ npm install
$ npm install -g gulp-cli gatsby-cli
$ npm run build:static
$ npm run start
```

### Development

Start development server

```sh
$ npm run start
```

Create a production build

```sh
$ npm run build
```

Optimize images from /src/staticSrc and place it in /static

```sh
$ npm run build:images
```

Watch files in src/staticSrc and automatically optimize and place it in /static

```sh
$ npm run watch:static
```
