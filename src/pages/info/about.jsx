import React from "react"
import Layout from "../../components/skeleton/Layout"
import DefaultPage from "../../components/defaultPage"

const About = () => {
  return (
    <Layout>
      <DefaultPage pageTitle="About" />
    </Layout>
  )
}

export default About
