import React from "react"
import { makeStyles } from "@material-ui/core/styles"
import Toolbar from "@material-ui/core/Toolbar"
import AppBar from "@material-ui/core/AppBar"
import { Link } from "gatsby"
import Typography from "@material-ui/core/Typography"

const useStyles = makeStyles(theme => {
  return {
    appBar: {
      top: "auto",
      bottom: 0,
      height: 36,
      display: "flex",
      alignItems: "center",
      "& a": {
        opacity: "60%",
        color: "white",
        padding: theme.spacing(1),
        paddingBottom: 60,
        height: 16,
        textDecoration: "none",
        paddingLeft: 0,
        "&:hover": {
          opacity: 1,
        },
      },
    },
  }
})

const footerLinks = [
  { to: "/about", value: "About" },
  { to: "/faqs", value: "FAQs" },
  { to: "/contact", value: "Contact Us" },
  { to: "/privacy", value: "Privacy Policy" },
]

const Footer = ({ children }) => {
  const classes = useStyles()
  return (
    <>
      <footer>
        <AppBar position="fixed" color="primary" className={classes.appBar}>
          <Toolbar className={classes.appBar}>
            {footerLinks.map((link, key) => {
              return (
                <Link key={key} to={"." + link.to}>
                  <Typography
                    variant="body1"
                    component="h2"
                    className={classes.typography}
                  >
                    {link.value} {footerLinks.length - 1 !== key ? "|" : ""}
                  </Typography>
                </Link>
              )
            })}
          </Toolbar>
        </AppBar>
      </footer>
    </>
  )
}

export default Footer
