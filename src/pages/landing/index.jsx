import React from "react"
import { Link } from "gatsby"
import {
  ChatBubbleOutline,
  HelpOutlineOutlined,
  ThumbUpAltOutlined,
  PanToolOutlined,
} from "@material-ui/icons"
import Button from "@material-ui/core/Button"
import Typography from "@material-ui/core/Typography"
import Layout from "../../components/skeleton/Layout"
import SEO from "../../components/skeleton/seo"
import LandingStyle from "./__style"
import { withPrefix } from "gatsby"

const LandingPage = () => {
  const classes = LandingStyle()
  return (
    <Layout>
      <SEO title="Landing" />
      <div className={classes.main}>
        <img
          className="sampleImg"
          alt=""
          src={withPrefix("images/landing/logo.png")}
        ></img>
        <div className={classes.desc}>
          <Typography
            variant="body2"
            component="h2"
            className={classes.typography}
          >
            Welcome to
          </Typography>
          <Typography
            variant="body1"
            component="h1"
            className={classes.typography}
          >
            TraceCOVID.ph
          </Typography>
        </div>

        <div className={classes.root}>
          <Link className={classes.buttons} to="/check">
            <Button variant="contained">
              <img
                alt=""
                src={withPrefix("images/landing/location_icon.png")}
              ></img>
              <Typography variant="body1" component="span">
                Check In
              </Typography>
            </Button>
          </Link>
          <Link className={classes.buttons} to="/map">
            <Button variant="contained">
              <img
                alt=""
                src={withPrefix("images/landing/test_kit_icon.png")}
                style={{ marginBottom: 10 }}
              ></img>
              <Typography variant="body1" component="span">
                View Trace Map
              </Typography>
            </Button>
          </Link>
        </div>
        <div className={classes.navigationButtons}>
          <Link to="/info/contact">
            <span>
              <ChatBubbleOutline />
            </span>
            Contact Us
          </Link>
          <Link to="/info//faqs">
            <span>
              <HelpOutlineOutlined />
            </span>
            View our FAQs
          </Link>
          <Link to="/info//about">
            <span>
              <ThumbUpAltOutlined />
            </span>
            Learn more about us
          </Link>
          <Link to="/info//privacy">
            <span>
              <PanToolOutlined />
            </span>
            Privacy Policy
          </Link>
        </div>
        <div className={classes.dctx}>
          <img
            alt=""
            src={withPrefix("images/landing/poweredByDCTx.png")}
          ></img>
        </div>
      </div>
    </Layout>
  )
}

export default LandingPage
