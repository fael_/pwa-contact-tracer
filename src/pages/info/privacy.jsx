import React from "react"
import Layout from "../../components/skeleton/Layout"
import DefaultPage from "../../components/defaultPage"

const Privacy = () => {
  return (
    <Layout>
      <DefaultPage pageTitle="Privacy" />
    </Layout>
  )
}

export default Privacy
